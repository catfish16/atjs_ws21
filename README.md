# Advanced topics of JS

This is the repository we started to fill as part of our winter semester course in 2021 at [FH St.Poelten](https://www.fhstp.ac.at/en/academic-studies-continuing-education/media-digital-technologies/creative-computing)

### This project includes

- super awesome calculator
- pokedex
- guess-the-colour card game
- modified version of my favourite card game, known as the Slappy game
- and stuff we did in the lectures

### How to start the app

To start the app switch to the `app` folder and run `npm install` to get all dependencies.
Then you can call `npm run dev` to run the dev server and have the application running.



### Check it out!
[![Netlify Status](https://api.netlify.com/api/v1/badges/cbeda766-fd94-4242-8c8c-53fbc3808406/deploy-status)](https://app.netlify.com/sites/atjs-cc201026/deploys)

The app is deployed on Netlify, enjoy!
https://atjs-cc201026.netlify.app/
