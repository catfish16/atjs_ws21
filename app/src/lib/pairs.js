import { useKeypress } from "vue3-keypress";
import { ref } from "vue";

export default {
    components: {
        KeyBackground,
    },
    setup() {
        const pressedKeyCode = ref(null);
        const isSuccess = ref(false);
        const isActiveRef = ref(true);

        const someSuccessCallback = ({ keyCode }) => {
            isSuccess.value = true;
        };

        const someWrongKeyCallback = ({ event }) => {
            isSuccess.value = false;
        };

        const someAnyKeyCallback = ({ event }) => {
            pressedKeyCode.value = event.keyCode;
        };

        useKeypress({
            keyEvent: "keydown",
            keyBinds: [
                {
                    keyCode: "left", // or keyCode as integer, e.g. 37
                    modifiers: ["shiftKey"],
                    success: someSuccessCallback,
                    preventDefault: true, // the default is true
                },
                {
                    keyCode: "right", // or keyCode as integer, e.g. 39
                    modifiers: ["shiftKey"],
                    success: someSuccessCallback,
                    preventDefault: true, // the default is true
                },
            ],
            onWrongKey: someWrongKeyCallback,
            onAnyKey: someAnyKeyCallback,
            isActive: isActiveRef,
        });

        return {};
    },
};