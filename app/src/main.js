import { createApp } from 'vue';
import router from './lib/router';
import store from './lib/store';
import './index.css';


const app = createApp({});

app.config.globalProperties.document = Document

app.use(router);
app.use(store);
app.mount('#app');
