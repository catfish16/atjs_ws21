module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}"
  ],
  theme: {
    fontFamily: {
      'sans': ['Caveat', 'cursive'],
      'nav': ['"Xanh Mono"', 'monospace'],
    },
    extend: {},
  },
  plugins: [],
}
